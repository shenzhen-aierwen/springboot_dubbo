package com.dalaoyang.api.impl;

import com.dalaoyang.api.UserService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;

@Service
public class UserServiceImpl implements UserService {

    @Value("${server.port}")
    private String port;

    @Value("${dubbo.protocol.port}")
    private String dubboPort;

    @Override
    public String testUser(Long userId, String version) {
        return "调用成功，端口是：" + port +
                "。版本号是：" + version +
                "，用户id：" + userId +
                "，dubbo端口：" + dubboPort;

    }
}
